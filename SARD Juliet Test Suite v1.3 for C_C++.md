# SARD Juliet Test Suite v1.3 for C/C++

## Limitations of the Test Cases
* Test cases are simpler than natural code   
* Frequencies of flaws and non-flawed constructs in the test cases may not reflect their frequencies in natural code  


The C test case code targets the C89 standard so that the test cases can be compiled and analyzed using a wide variety of tools that may not support newer versions of the C language.  

The test cases cover 11 of the 2011 CWE/SANS Top 25 Most Dangerous Software Errors. Of the 14 CWE entries in the Top 25 that the test cases do not cover, 10 are design issues that do not fit into the structure of the CAS test cases. The other four are not specific to C/C++ and are covered in the related Java test cases.   

## Naming Scheme

* The identifying number and possibly shortened name of the CWE entry most closely associated with the intentional flaw.  

### functional variant
 This word or phrase is used to differentiate test cases for the same CWE entry If there is only one type of issue for a CWE entry, then the functional variant name for test cases for that CWE entry is “basic.”  

**Key Strings in Functional Variant Names**  
*  “w32” – This string in the functional variant name for a test case indicates that the functional variant is specific to the Windows operating system.   

### flow variant (Type of complexity)

 A two-digit number associated with a “flow variant” which indicates the type of data and/or control flow used in the test case. For example, flow variant “01” is the simplest form of the flaw and contains neither data nor control flows.  

Test cases with a flow variant of “01” are the simplest form of the flaws and do not contain
added control or data flow complexity. This set of test cases is referred to as the “Baseline” test
cases.  
 
Test cases with a flow variant other than “01” are referred to as the “More Complex” test cases. Those with a flow variant from “02” to “22” (inclusive) cover various types of control flow constructs and are referred to as the “Control Flow” test cases.  
 
Those with a flow variant of “31” or greater cover various types of data flow constructs and are referred to as the “Data Flow” test cases.  
 
The gap between 22 and 31 is left to allow for future expansion.
 
### Examples:
- CWE476_NULL_Pointer_ Dereference__char_01
```
“CWE” + CWE ID + “_” +  Shortened CWE entry name + “__” (two underscores) + Functional Variant
Name + “_” + Flow Variant + Sub-file Identifier + "." + Language identifier / file extension
```
The test case name example:  
**CWE190_Integer_Overflow__char_fscanf_add_01.c**  

		CWE Entry ID: 190  
		Shortened CWE Entry Name: “Integer_Overflow”  
		
		Functional Variant: “char_fscanf_add”  
		
		Flow Variant: 01  
	  	
		Language: C  

Examples:  
```
C test case CWE476_NULL_Pointer_Dereference__char_01 consists of one file:  
- CWE476_NULL_Pointer_Dereference__char_01.c  

C test case CWE476_NULL_Pointer_Dereference__char_51 consists of two files:  
- CWE476_NULL_Pointer_Dereference__char_51a.c  
- CWE476_NULL_Pointer_Dereference__char_51b.c  

C++ test case CWE563_Unused_Variable__unused_class_member_value_01 consists of two
files:  
- CWE563_Unused_Variable__unused_class_member_value_01_bad.cpp  
- CWE563_Unused_Variable__unused_class_member_value_01_good1.cpp  
```
## Sub-file Identifier
The simpler forms of most flaws can be contained in a single source code file, but some test
cases consist of multiple files. There are several reasons a test case may be split into multiple
files and each one uses a different type of string to identify each file in the test case.  

**class-based flaws:** Some C++ flaws are inherent in a class and require separate files for the flawed and  nonflawed constructs. In this case, the flaw will be in a file identified with the string “_bad” (such as  CWE401_Memory_Leak__destructor_01_ bad.cpp”) and the non-flaw will be in the file identified with the string “_good1” (such as  “CWE401_Memory_Leak__destructor_01_good1.cpp”).  

Some **Data Flow** test cases involve the **flow of data between functions in different source code files**. In these test cases, the test case will “start” in the file identified with the string “a,” such as  “CWE476_NULL_Pointer_Dereference__char_ 54a.c.” Functions in the “a” file will call functions in the “b” file, which may call functions in the “c” file, etc.   

Some **Data Flow** test cases involve the **flow of data between virtual function calls**. In the C++ version of these test cases, a header file (.h) is used to define the virtual function and implementations occur in separate source (.cpp) files.   

Some **Data Flow** test cases involve the **flow of data between a class constructor and destructor**. In these test cases, a header file (.h) is used to define the constructor and destructor and the implementations occur in separate source (.cpp) files  

---

# Test Case Design

 All C/C++ test cases also define a “main” function in the primary file. This main function is not
used when multiple test cases are compiled at once. However, it can be used when building an
individual test case, such as for developer testing or for creating binaries to use in testing binary
analysis tools.  

In the C/C++ test cases, the preprocessor macro INCLUDEMAIN must be defined at compile
time for this main function to be included in the compilation  

Most test cases cover flaws that can be contained in arbitrary functions (non-class-based flaws).
However, some flaws, called class-based flaws, are inherent in the C++ class definition and must
be handled differently in the test case design. An example of a class-based flaw is:
C++ test case CWE416_Use_After_Free__operator_equals_01
(In this test case, failure to define operator= may result in a program crash due to
potentially using memory after it has been freed.)  

Virtual function, constructor/destructor, and bad-only test cases are unique. Virtual function and
constructor/destructor test cases require multiple files while bad-only test cases are only used to
test flaws, as opposed to testing both flaws and non-flaws as in all other test cases.

 
## non-class-based flaw

### Required Functions

Test cases for flaws that are not inherent in a C++ class must define bad and good functions.
(Note: A few test cases are considered bad-only and do not contain an implementation of the
good function.)  
For test cases that use multiple files, the following functions are defined in the “a” sub-file.  
The “primary file” for a test case is a general term for the “a” sub-file in multi-file test cases, or the only file in single-file test cases.

#### Primary bad Functions 
Each test case contains exactly one primary bad function in the primary file. In many simpler test cases, this function contains the flawed construct, but in other test cases this function calls  other “sink” or “helper” function(s) that contain the flaw (“sink” and “helper” functions are described in a later section).

The primary bad function:
*  For C, is named with the test case name followed by the string “_bad,” such as
   “CWE78_OS_Command_Injection__char_connect_socket_execl_01_bad().”
* For C++, is named bad() and is in a namespace that is unique to the test case. The function is not part of a  C++ class.
* Takes no parameters and has no return value.
* The name of the primary bad function matches the following regular expression:
```
   ^(CWE.*_)?bad$  
```
####  The primary good function:

Each test case contains exactly one primary good function in the primary file (the same file as the primary bad function).  The only code in this good function is a call to each of the secondary good functions.  

The name of the primary good function matches the following regular expression:   
```
^(CWE.*_)?good$  
```
#### Secondary Good Function(s)
Non-class-based test cases also contain one or more secondary good functions in the primary file. Some of the bad-only test cases, however, do not include any secondary good functions.   

In many simpler test cases, these secondary good functions contain the actual non-flawed constructs.   

In other test cases, these functions will call “sink” or “helper” functions, which contain the non-flawed constructs. The number of secondary good functions depends on the test case’s flaw type as well as how many non-flawed constructs similar to that flaw exist.     

Many test cases have only one secondary good function, but others may have more.

There are three naming conventions used for   secondary good functions:  

* goodG2B, goodG2B1, goodG2B2, goodG2B3, etc.   	- These names are used in data flow test cases when a **good source is passing safe data to a potentially bad sink**.  
*  goodB2G, goodB2G1, goodB2G2, goodB2G3, etc.     – These names are used in data flow test cases when a **bad source is passing unsafe or potentially unsafe data to a good sink**.  
*  good1, good2, good3, etc.   
– This is the “default” or “generic” name for these functions when the conditions above do not apply.   

The names of the secondary good functions match the following regular expression:   
```
^good(\d+|G2B\d*|B2G\d*)$ 
```
>	Note: It is important that this regular expression does not overlap with the previously defined good function regular expression so that the primary good functions are not matched. 

**The secondary good functions have the same argument and return types as the primary bad and primary good functions. ** In addition, the secondary good functions have the following characteristics: 
> In C and C++ test cases, the secondary good functions are **statically scoped**. Therefore, they are only accessible within that source code file, which prevents name collisions. 
>  In C++ test cases, the secondary good functions are in the **namespace that is unique to the test case**. 
>  The functions are **not part of a C++ class**.

**Optional Functions **   

In addition to the required functions, test cases may define “helper,” “source,” and/or “sink” functions.    

**Helper Functions:** Helper functions are used in test cases when even the simplest form of the flaw cannot be contained in a single function (within the constraints of the test case design).   
Functions used to create data flow patterns (“source” and “sink” functions) in More Complex test cases are not considered “helper” functions because they are not part of the flaw construct. Examples of test cases where helper functions are required include:  Test cases involving variable argument functions, such as in the C test case CWE134_Uncontrolled_Format_String__char_console_vprintf_01.  Test cases for unused parameter, such as in the C test case CWE563_Unused_Variable__unused_parameter_variable_01.


# class-based flaw

# virtual function 

# constructor/destructor and bad-only test cases.
